package com.example.amitkrishna.alarmmeloc;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Locale;

public class SetAlarmLocation extends AppCompatActivity  implements LocationListener{
//Locaation Manager Provides the Longitude and Longitude on current Location
    public TextView textView;
    public double distance;
    public TextView locationText;
    int PLACE_PICKER_RESULT=1;
    LocationManager locationManager;
    public Button button;
    public Button set_location;
    public Button exit;
    double placepicker_long;
    double placepicker_lat;
    public TextView Text_distance;
    public TextToSpeech textToSpeech;
    public int flag=0,flag2=0;
    public String Place;

        //This code Snippet initializes the PlacePicker API
        //onCreate method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm_location);
        getSupportActionBar().hide();

        textView = (TextView) findViewById(R.id.textView);

        locationText = (TextView) findViewById(R.id.location);

        button = (Button) findViewById(R.id.button);

        set_location = (Button) findViewById(R.id.button_setlocation);

        Text_distance = (TextView) findViewById(R.id.dist);

        exit = (Button) findViewById(R.id.button_exit);
        //As higher versions of Android need in app permission so we will request in app permission this way-

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 1;
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();


                Intent intent;
                try {
                    intent = (Intent) builder.build(SetAlarmLocation.this);
                    startActivityForResult(intent, PLACE_PICKER_RESULT);
                } catch (GooglePlayServicesRepairableException e) {
                    Toast.makeText(getApplicationContext(), "Sorrey", Toast.LENGTH_LONG).show();
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(getApplicationContext(), "App Failed", Toast.LENGTH_LONG).show();
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                }

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch (Exception ex) {
                }

                if (!gps_enabled && !network_enabled)
                {
                    Toast.makeText(getApplicationContext(),"Please Enable Your Location!!",Toast.LENGTH_LONG).show();
                    textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                        @Override
                        public void onInit(int status) {
                            if (status != TextToSpeech.ERROR) {
                                textToSpeech.setLanguage(Locale.ENGLISH);
                                textToSpeech.speak("Please Turn ON the Location Of your Device!!", TextToSpeech.QUEUE_FLUSH, null);
                            }
                        }
                    });

                }

                if (flag == 1) {
                    getLocation();
                } else {
                    Toast.makeText(getApplicationContext(), "Please Your Destination First ", Toast.LENGTH_LONG).show();
                    textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                        @Override
                        public void onInit(int status) {
                            if (status != TextToSpeech.ERROR) {
                                textToSpeech.setLanguage(Locale.ENGLISH);
                                textToSpeech.speak("Please Select Your Destination First!!", TextToSpeech.QUEUE_FLUSH, null);
                            }
                        }
                    });
                }
            }
        });


        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }

    void getLocation()
    {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        locationText.setText("Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude());

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+ addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            distance=distance(location.getLatitude(),location.getLongitude(),placepicker_lat,placepicker_long);
            Text_distance.setText("You are "+distance+" KMs away from the Location");
            if(distance<1)
            {
                AlarmManager alarmMgr;
                PendingIntent alarmIntent;

                alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(SetAlarmLocation.this, Alarm_Receiver.class);
                alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

                alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + 1000, alarmIntent);
                flag2=1;
                if(flag2==1) {
                    Intent intent1 = new Intent(SetAlarmLocation.this, Display.class);
                    Place=textView.getText().toString();
                    intent1.putExtra("address",Place);
                    startActivity(intent1);
                }

//Wake up the device to fire a one-time (non-repeating) alarm in one minute:

            }

        }catch(Exception e)
        {
            Toast.makeText(getApplicationContext(),"Sorry Exception",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    //Place Picker
    //a code snippet useful for Place Picker
    protected void onActivityResult(int request_Code,int result_Code,Intent data)
    {
        LatLng longlat;
        if(request_Code==PLACE_PICKER_RESULT)
        {
            if(result_Code==RESULT_OK)
            {
                Place place=PlacePicker.getPlace(data,this);
                String address=String.format("Place: %s",place.getAddress());
                longlat=place.getLatLng();
                textView.setText(address+" Latitude:"+longlat.latitude+" Longitude:"+longlat.longitude);
                placepicker_lat=longlat.latitude;
                placepicker_long=longlat.longitude;

            }

        }

    }
    //The function Gives the Distance between two points in km(kilometers)
    //Distance Finding Function

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
