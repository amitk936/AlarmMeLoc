package com.example.amitkrishna.alarmmeloc;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Main3Activity extends AppCompatActivity {
    private Button button;
    private ImageView imageone;//set By Time
    private ImageView imagetwo;// set By Location

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"Have a good Day",Toast.LENGTH_SHORT);
                AlertDialog.Builder builder=new AlertDialog.Builder(Main3Activity.this);
                builder.setCancelable(true);
                builder.setTitle("Back");
                builder.setMessage("Do you want go back");

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });
                builder.show();
            }
        });

        //imageone=(ImageView)findViewById(R.id.imgone);//SET BY TIME
        imagetwo=(ImageView)findViewById(R.id.imgtwo);//SET BY LOCATION

       /* imageone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set by time Activity
                Intent intent=new Intent(Main3Activity.this,Insert.class);
                startActivity(intent);
            }
        });*/

        imagetwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set by Location Activity
                Intent intent=new Intent(Main3Activity.this,SetAlarmLocation.class);
                startActivity(intent);
            }
        });





    }
}
