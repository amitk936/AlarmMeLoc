package com.example.amitkrishna.alarmmeloc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Amit Krishna on 01/04/2018.
 */

public class Alarm_Receiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent service_intent=new Intent(context,RingtonePlayingService.class);

        context.startService(service_intent);
    }
}
